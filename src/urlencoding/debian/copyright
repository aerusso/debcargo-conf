Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: urlencoding
Upstream-Contact:
 Kornel <kornel@geekhood.net>
 Bertram Truong <b@bertramtruong.com>
Source: https://github.com/kornelski/rust_urlencoding

Files: *
Copyright:
 2020-2023 Kornel <kornel@geekhood.net>
 2016—2023 Bertram Truong <b@bertramtruong.com>
License: MIT

Files: debian/*
Copyright:
 2019-2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019-2023 Andrej Shadura <andrewsh@debian.org>
 2022-2023 Alexander Kjäll <alexander.kjall@gmail.com>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
